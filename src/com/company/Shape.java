package com.company;

public interface Shape {
    public void draw();
    public void resizeByPercentage(double pct);
}
