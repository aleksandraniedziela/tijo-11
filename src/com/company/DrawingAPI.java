package com.company;

public interface DrawingAPI {
    public void drawCircle(double x, double y, double radius);
}
