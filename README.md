## Wzorzec projektowy MOST
   Należy do grupy wzorców strukturalnych. Wzorzec ten powstał by oddzielić interfejs od implementacji tak by oba elementy
   mogły istnieć niezależnie, a co za tym idzie by powstała możliwość wprowadzania zmian do implementacji bez konieczności zmian w kodzie,
   który korzysta z klasy.

## Korzyści ze stosowania wzorca mostu:

 - umożliwia odseparowanie implementacji od interfejsu,
 - poprawia możliwość rozbudowy klas,
 - ukrywanie szczegółów implementacyjnych od klienta.

## Stosowalność:

 - wymagana jest oddzielność interfejsu od implementacji,
 - zarówno interfejs jak i implementacja musi zostać rozbudowana poprzez podklasy,
 - zmiana implementacyjne nie mogą mieć wpływu na klienta.

## Klasy które uczestniczą w tym wzorcu:

 - Abstraction - definiuje interfejs abstrackji oraz posiada referencję do implementacji.
 - RefinedAbstraction - rozszerza interfejs abstrakcji.
 - Implementator - definiuje interfejs implementatora. Interfejs ten nie musi być zgodny z abstrakcją.
   Przeważnie Implementator dostarcza niskopoziomowego interfejsu, a abstrakcja operuje na tym interfejsie.
 - ConcreteImplementator - implementuje interfejs Implementatora.

  Przykład:

 Wyobraźmy sobie abstrakcję, jaką jest figura. Można ją wyszczególnić na np. kwadraty, czy  trójkąty,
 jednak są pewne metody dla każdej figury jak np. rysowanie. Jednak rysowanie może być różne dla różnych
 bibliotek graficznych czy systemów operacyjnych.Wzorzec mostu pozwala na stworzenie nowych klas, które dostarczają
 konkretnych implementacji do rysowania. Klasa abstrakcyjna figury dostarcza informacji o figurze(np.wielkość),podczas
 gdy implementacja dostarcza interfejs do rysowania



 [Zródło: http://zasoby.open.agh.edu.pl/~09sbfraczek/most%2C1%2C36.html]
 [Źródło: http://patryknet.blogspot.com/2011/12/bridge-pattern.html]
